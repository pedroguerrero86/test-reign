const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cron = require('node-cron')
const mongoose = require('mongoose')
const cors = require('cors')

const ArticleJob = require('./app/jobs/articleJob')
const routes = require('./app/routes')
const errorMiddleware = require('./app/middleware/error')
const routeNotFoundMiddleware = require('./app/middleware/routeNotFound')

const app = express()

cron.schedule('0 * * * *', () => {
  ArticleJob.get()
})

app.use(logger('dev'))
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use('/api/v1', routes)
app.use(routeNotFoundMiddleware)
app.use(errorMiddleware)

const user = process.env.DB_USER
const password = process.env.DB_USER_PASSWORD
const host = process.env.DB_HOST
const port = process.env.DB_PORT
const database = process.env.DB_NAME

mongoose
  .connect(`mongodb://${user}:${password}@${host}:${port}/${database}`)
  // eslint-disable-next-line no-unused-vars
  .then(result => {
    app.listen(3000)
  })
  .catch(err => {
    console.log(err)
  })
