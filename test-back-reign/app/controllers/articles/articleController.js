const { validationResult } = require('express-validator')

const Article = require('../../models/article')

class ArticleController {
  static async index(req, res, next) {
    try {
      const articles = await Article.find({
        deleted: false
      })

      res.status(200).json(articles)
    } catch(err) {
      const error = new Error('Error getting the articles')
      next(error)
    }
  }

  static async destroy(req, res, next) {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    try {
      const { params: { id } } = req
      const article = await Article.findById(id)

      if (article.deleted) {
        const error = new Error(`article with id ${id} not found`)
        error.status = 404

        return next(error)
      }

      article.deleted = true
      await article.save()

      res.status(200).json(article)
    } catch(err) {
      const error = new Error('Error deleting the article')

      next(error)
    }
  }
}

module.exports = ArticleController
