const axios = require('axios')

const instance = axios.create({
  baseURL: 'https://hn.algolia.com'
})

module.exports = instance
