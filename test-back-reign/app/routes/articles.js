const { Router } = require('express')

const deleteArticleValidation = require('../validations/article/deleteArticle')

const ArticleController = require('../controllers/articles/articleController')

const router = Router()

router.get('/', ArticleController.index)

router.delete('/:id', deleteArticleValidation, ArticleController.destroy)

module.exports = router
