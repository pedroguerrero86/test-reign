const mongoose = require('mongoose')

const Schema = mongoose.Schema

const articleSchema = new Schema({
  objectId: String,
  storyId: Number,
  title: String,
  author: String,
  url: String,
  date: Date,
  deleted: Boolean
})

module.exports = mongoose.model('Article', articleSchema)
