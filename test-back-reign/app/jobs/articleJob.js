const hkClient = require('../httpClients/hackerNews')
const Article = require('../models/article')

class ArticleJob {
  static async get() {
    try {
      const response = await hkClient.get('/api/v1/search_by_date', {
        params: {
          query: 'nodejs'
        }
      })
      const { data: { hits } } = response

      for (let hit of hits) {
        const article = await Article.find({
          objectId: hit.objectID
        })

        if (article.length > 0) {
          continue
        }

        if (!hit.story_title && !hit.title) {
          continue
        }

        const newArticle = new Article({
          objectId: hit.objectID,
          storyId: hit.story_id,
          title: hit.story_title || hit.title,
          author: hit.author,
          url: hit.story_url,
          date: hit.created_at,
          deleted: false
        })

        await newArticle.save()
      }
    } catch(err) {
      console.log('Error getting the articles')
      console.log(err)
    }
  }
}

module.exports = ArticleJob
