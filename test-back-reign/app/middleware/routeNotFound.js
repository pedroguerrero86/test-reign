const { Router } = require('express')

const router = Router()

router.use('*', (req, res, next) => {
  const error = new Error('route not found')
  error.status = 404

  next(error)
})

module.exports = router
