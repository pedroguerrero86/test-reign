// eslint-disable-next-line no-unused-vars
module.exports = (err, req, res, next) => {
  const httpCode = err.status || 500

  res.status(httpCode).json({message: err.message})
}
