import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';

import { ArticleService } from '../../services/article.service';
import { Article } from '../../interfaces/article.interface';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @ViewChild(MatTable, { static: false }) table: MatTable<any>;
  displayedColumns: string[] = ['title', 'date', 'action'];
  dataSource: Article[] = [];

  constructor(
    private articleService: ArticleService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.articleService.getArticles().subscribe((data: Article[]) => {
      this.dataSource = [...data];
    });
  }

  deleteArticle(event: Event, id: string): void {
    event.stopPropagation();

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      const articleFound = this.dataSource.find(article => article._id === id);
      if (!articleFound) {
        return;
      }

      this.articleService.deleteArticle(id).subscribe((article: Article) => {
        const index = this.dataSource.indexOf(articleFound);

        this.dataSource.splice(index, 1);
        this.table.renderRows();
      });
    });
  }

  onClickRow(data: Article) {
    window.open(data.url);
  }
}
