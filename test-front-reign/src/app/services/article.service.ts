import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from '../interfaces/article.interface';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private articles: Article[] = [];
  constructor(private httpClient: HttpClient) {}

  getArticles() {
    return this.httpClient.get<Article[]>(
      'http://localhost:5757/api/v1/articles'
    );
  }

  deleteArticle(id: string) {
    return this.httpClient.delete<Article>(
      `http://localhost:5757/api/v1/articles/${id}`
    );
  }
}
