export interface Article {
  _id: string;
  objectId: string;
  storyId: number;
  title: string;
  author: string;
  url: string;
  date: Date;
  deleted: boolean;
}
