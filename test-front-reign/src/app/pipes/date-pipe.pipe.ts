import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'datePipe'
})
export class DatePipePipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    const date = new Date(value);
    const now = new Date();
    const months: string[] = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Nov',
      'Dec'
    ];

    if (
      date.getDate() === now.getDate() &&
      date.getMonth() === now.getMonth() &&
      date.getFullYear() === now.getFullYear()
    ) {
      let hour = date.getHours();
      let suffix = 'am';
      if (hour > 12) {
        hour -= 12;
        suffix = 'pm';
      }
      return `${hour}:${date.getMinutes()} ${suffix}`;
    }

    now.setDate(now.getDate() - 1);

    if (
      date.getDate() === now.getDate() &&
      date.getMonth() === now.getMonth() &&
      date.getFullYear() === now.getFullYear()
    ) {
      return 'Yesterday';
    }

    return `${months[date.getMonth()]} ${date.getDate()}`;
  }
}
